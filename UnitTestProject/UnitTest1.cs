﻿using NUnit.Framework;
using examen.ViewModels;

namespace UnitTestProject
{
    [TestFixture]
    public class Tests
    {
        UniversitiesViewModel viewModel;

        [SetUp]
        public void Setup()
        {
            viewModel = new UniversitiesViewModel();
        }

        [Test]
        public async void Test1()
        {
            var universities = await viewModel.GetUniversitiesUnit();
            Assert.IsNotNull(universities, "universities are emptys");
        }

        [Test]
        public void Test2()
        {
            Assert.IsNotNull(viewModel.Package, "the depency service is null");
        }
    }
}
