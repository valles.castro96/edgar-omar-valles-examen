﻿using System;
using examen.Interfaces;
using examen.iOS.Services;
using Foundation;
using Xamarin.Forms;

[assembly: Dependency(typeof(PackageNameService))]
namespace examen.iOS.Services
{
    public class PackageNameService : IPackageName
    {
        public string PackageName()
        {
            return NSBundle.MainBundle.BundleIdentifier;
        }
    }
}

