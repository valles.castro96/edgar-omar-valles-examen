﻿using System;
using examen.Droid.Services;
using examen.Interfaces;
using Xamarin.Forms;

[assembly: Dependency(typeof(PackageNameService))]
namespace examen.Droid.Services
{
    public class PackageNameService : IPackageName
    {

        public string PackageName()
        {
            return Android.App.Application.Context.PackageName;
        }
    }
}

