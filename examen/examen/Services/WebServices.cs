﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using examen.Interfaces;
using examen.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace examen.Services
{
    public class WebServices : IWebServices
    {
        string sContentType = "application/json";
        JObject jObject = new JObject();
        MemoryStream m = new MemoryStream();

        public async Task<ObservableCollection<UniversitiesModel>> GetUniversities()
        {
            if (CheckInternetService.IsInternet())
            {
                var client = new HttpClient();
                var oTaskGetAsync = await client.GetAsync("http://universities.hipolabs.com/search?country=United+States");
                if (oTaskGetAsync.IsSuccessStatusCode)
                {
                    var response = oTaskGetAsync.Content.ReadAsStringAsync().Result;
                    return JsonConvert.DeserializeObject<ObservableCollection<UniversitiesModel>>(response);
                }
            }
            return null;
        }
    }
}

