﻿using System;
using System.Collections.Generic;

namespace examen.Models
{
    public class UniversitiesModel
    {
        public List<string> domains { get; set; }
        public string alpha_two_code { get; set; }
        public List<string> web_pages { get; set; }
        public string country { get; set; }
        public string name { get; set; }
    }
}

