﻿using System;
using examen.Models;
using System.Threading.Tasks;
using System.Collections.ObjectModel;

namespace examen.Interfaces
{
    interface IWebServices
    {
        Task<ObservableCollection<UniversitiesModel>> GetUniversities();
    }
}

