﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Input;
using Acr.UserDialogs;
using examen.Interfaces;
using examen.Models;
using examen.Services;
using Plugin.Media;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace examen.ViewModels
{
    public class UniversitiesViewModel : BaseViewModel
    {
        #region Variables
        ObservableCollection<UniversitiesModel> _universities;
        public ObservableCollection<UniversitiesModel> Universities
        {
            get { return _universities; }
            set
            {
                _universities = value;
                NotifyPropertyChanged("Universities");
            }
        }

        bool isBusy;
        public bool IsBusy
        {
            get { return isBusy; }
            set
            {
                isBusy = value;
                NotifyPropertyChanged("IsBusy");
            }
        }

        bool _isVisible = false;
        public bool IsVisible
        {
            get { return _isVisible; }
            set
            {
                _isVisible = value;
                NotifyPropertyChanged("IsVisible");
            }
        }

        string _package;
        public string Package
        {
            get { return _package; }
            set
            {
                _package = value;
                NotifyPropertyChanged("Package");
            }
        }

        ImageSource _photo;
        public ImageSource Photo
        {
            get { return _photo; }
            set
            {
                _photo = value;
                NotifyPropertyChanged("Photo");
            }
        }

        private IUserDialogs dialogs;
        IWebServices services = new WebServices();

        public ICommand VisitWebPageCommand { get; set; }
        public ICommand TakePhotoCommand { get; set; }
        #endregion

        public UniversitiesViewModel()
        {
            dialogs = UserDialogs.Instance;
            Package = DependencyService.Get<IPackageName>().PackageName();
            VisitWebPageCommand = new Command<UniversitiesModel>(VisistWebPage);
            TakePhotoCommand = new Command(() => TakePhoto());
        }

        private async void TakePhoto()
        {
            await CrossMedia.Current.Initialize();

            if(!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
            {
                await dialogs.AlertAsync(message: "This device don't have camera",
                    title: "No camera avaliable", okText: "Aceptar");
                return;
            }

            var file = await CrossMedia.Current.TakePhotoAsync(new Plugin.Media.Abstractions.StoreCameraMediaOptions
            {
                PhotoSize = Plugin.Media.Abstractions.PhotoSize.Small,
                CompressionQuality = 100
            });

            if (file == null)
            {
                await dialogs.AlertAsync("You don't take a photo", okText: "OK");
                return;
            }
            Photo = ImageSource.FromStream(() =>
            {
                var stream = file.GetStream();
                return stream;
            });

            IsVisible = Photo != null;
            NotifyPropertyChanged("IsVisible");
        }

        private async void VisistWebPage(UniversitiesModel obj)
        {
            if (await dialogs.ConfirmAsync($"Are you sure visit the website of the {obj.name}?",
                okText: "YES", cancelText: "NO"))
            {
                await Browser.OpenAsync(obj.web_pages[0],
                    new BrowserLaunchOptions
                    {
                        LaunchMode = BrowserLaunchMode.SystemPreferred,
                        TitleMode = BrowserTitleMode.Show,
                        PreferredControlColor = Color.FromHex("#4dc8f3"),
                        PreferredToolbarColor = Color.FromHex("#c6d5e8")
                    });
            }
        }

        private async static Task enviarCorreo(Data usuario)
        {
            await UserDialogs.Instance.AlertAsync(message: $"Enviar mensaje a {usuario.last_name} {usuario.first_name}", "Mensaje","Aceptar");
        }

        public async void GetUniversities()
        {
            if (isBusy || Universities != null)
                return;

            isBusy = true;
            using (dialogs.Loading("Getting Colleges", maskType: MaskType.Black))
            {
                ObservableCollection<UniversitiesModel> d = new ObservableCollection<UniversitiesModel>();
                var json = await services.GetUniversities();
                if (json != null)
                {
                    foreach(var item in json)
                    {
                        d.Add(item);
                    }
                    Universities = d;
                }
                else
                {
                    await dialogs.AlertAsync(message: "Something went wrong, please try again later", title: "Error", okText: "OK");
                }
                isBusy = false;
            }
        }


        public async Task<ObservableCollection<UniversitiesModel>> GetUniversitiesUnit()
        {
            using (dialogs.Loading("Getting Colleges", maskType: MaskType.Black))
            {
                ObservableCollection<UniversitiesModel> d = new ObservableCollection<UniversitiesModel>();
                var json = await services.GetUniversities();
                if (json != null)
                {
                    foreach (var item in json)
                    {
                        d.Add(item);
                    }
                }
                else
                {
                    await dialogs.AlertAsync(message: "Something went wrong, please try again later", title: "Error", okText: "OK");
                }
                return d;
            }
        }
    }
}

