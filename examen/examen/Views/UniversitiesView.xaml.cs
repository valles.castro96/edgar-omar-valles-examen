﻿using System;
using System.Collections.Generic;
using examen.ViewModels;
using Xamarin.Forms;

namespace examen.Views
{
    public partial class UniversitiesView : ContentPage
    {
        UniversitiesViewModel viewModel;

        public UniversitiesView()
        {
            InitializeComponent();
            BindingContext = viewModel = new UniversitiesViewModel();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            viewModel.GetUniversities();
        }
    }
}

